import React, { useEffect, useState } from 'react';

function ConferenceForm() {
  const [locations, setLocations] = useState([])

  const [formData, setFormData] = useState({
    name: '',
    starts: '',
    ends: '',
    description: '',
    max_presentations: '',
    max_attendees: '',
    location: '',
  })

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8000/api/conferences/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;


    setFormData({
      ...formData,

      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">

            <div className="form-floating mb-3">
              <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <input value={formData.starts} onChange={handleFormChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>

            <div className="form-floating mb-3">
              <input value={formData.ends} onChange={handleFormChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>

            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <textarea value={formData.description} onChange={handleFormChange} className="form-control" id="description" rows="3" name="description"></textarea>
            </div>

            <div className="form-floating mb-3">
              <input value={formData.max_presentations} onChange={handleFormChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>

            <div className="form-floating mb-3">
              <input value={formData.max_attendees} onChange={handleFormChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>

            <div className="mb-3">
              <select value={formData.location} onChange={handleFormChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>{location.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ConferenceForm;


// import React, { useEffect, useState } from 'react';
// function ConferenceForm() {
//     const [name, setName] = useState('')
//   const [starts, setStarts] = useState('')
//   const [ends, setEnds] = useState('')
//   const [description, setDescription] = useState('')
//   const [max_presentations, setMaxPresentations] = useState('')
//   const [max_attendees, setMaxAttendees] = useState('')
//   const [location, setLocation] = useState('')
//   const handleSubmit = async (event) =>{
//     event.preventDefault();
//     const data = {}
//     data.starts = starts;
//     data.name = name;
//     data.ends = ends;
//     data.description = description;
//     data.max_presentations = max_presentations;
//     data.max_attendees = max_attendees;
//     data.location = location;
//     console.log(data)
//     const conferenceUrl = 'http://localhost:8000/api/conferences/';
//     const fetchConfig = {
//     method: "post",
//     body: JSON.stringify(data),
//     headers: {
//       'Content-Type': 'application/json',
//     },
//   };
//   const response = await fetch(conferenceUrl, fetchConfig);
//   if (response.ok) {
//     const newConference = await response.json();
//     console.log(newConference);
//     setName('')
//     setStarts('')
//     setEnds('')
//     setDescription('')
//     setMaxPresentations('')
//     setMaxAttendees('')
//     setLocation('')
//   }
//   }
//   const handleNameChange = (e) => {
//     setLocation(e.target.value)
//   }
//   const handleStartsChange = (e) => {
//     setLocation(e.target.value)
//   }
//   const handleEndsChange = (e) => {
//     setLocation(e.target.value)
//   }
//   const handleDescriptionChange = (e) => {
//     setLocation(e.target.value)
//   }
//   const handleMaxPresentationsChange = (e) => {
//     setLocation(e.target.value)
//   }
//   const handleMaxAttendeesChange = (e) => {
//     setLocation(e.target.value)
//   }
//   const handleLocationChange = (e) => {
//     setLocation(e.target.value)
//   }
//   const [locations, setLocations] = useState([])
//   const fetchData = async () => {
//     const url = 'http://localhost:8000/api/locations/';
//     const response = await fetch(url);
//     if (response.ok) {
//       const data = await response.json();
//       setLocations(data.locations)
//     }
//   }
//   useEffect(() => {
//     fetchData();
//   }, []);
//   return(
//     <div className="row">
//         <div className="offset-3 col-6">
//           <div className="shadow p-4 mt-4">
//             <h1>Create a new conference</h1>
//             <form onSubmit={handleSubmit} id="create-conference-form">
//               <div className="form-floating mb-3">
//                 <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
//                 <label htmlFor="name">Name</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input value={starts} onChange={handleStartsChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
//                 <label htmlFor="starts">Starts</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input value={ends} onChange={handleEndsChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
//                 <label htmlFor="ends">Ends</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input value={description} onChange={handleDescriptionChange} placeholder="Description" required type="text" name="description" id="description" className="form-control" />
//                 <label htmlFor="description">Description</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input value={max_presentations} onChange={handleMaxPresentationsChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
//                 <label htmlFor="max_presentations">Maximum presentations</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input value={max_attendees} onChange={handleMaxAttendeesChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
//                 <label htmlFor="max_attendees">Maximum attendees</label>
//               </div>
//               <div className="mb-3">
//                 <select required value={location} onChange={handleLocationChange} name="Location" id="location" className="form-select">
//                   <option value="">Choose a Location</option>
//                   {locations.map(location => {
//                         return (
//                         <option key={location.href} value={location.href}>
//                             {location.name}
//                         </option>
//                         );
//                     })}
//                 </select>
//               </div>
//               <button className="btn btn-primary">Create</button>
//             </form>
//           </div>
//         </div>
//       </div>
//   )
// }
// export default ConferenceForm;

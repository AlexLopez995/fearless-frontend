import React, { useEffect, useState } from 'react';
function LocationForm() {
    const [name, setName] = useState('')
  const [roomCount, setRoomCount] = useState('')
  const [city, setCity] = useState('')
  const [state, setState] = useState('')
  const handleSubmit = async (event) =>{
    event.preventDefault();
    const data = {}
    data.room_count = roomCount;
    data.name = name;
    data.city = city;
    data.state = state;
    console.log(data)
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const response = await fetch(locationUrl, fetchConfig);
  if (response.ok) {
    const newLocation = await response.json();
    console.log(newLocation);
    setName('')
    setRoomCount('')
    setCity('')
    setState('')
  }
  }
  const handleNameChange = (e) => {
    setName(e.target.value)
  }
  const handleCityChange = (e) => {
    setCity(e.target.value)
  }
  const handleRoomCountChange = (e) => {
    setRoomCount(e.target.value)
  }
  const handleStateChange = (e) => {
    setState(e.target.value)
  }
  const [states, setStates] = useState([])
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setStates(data.states)
    }
  }
  useEffect(() => {
    fetchData();
  }, []);
  return(
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={roomCount} onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input value={city} onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" className="form-control" />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select required value={state} onChange={handleStateChange} name="state" id="state" className="form-select">
                  <option value="">Choose a state</option>
                  {states.map(state => {
                        return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                        </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}
export default LocationForm;





// import React, {useEffect, useState} from 'react';

// function LocationForm (props) {
//     const [states, setStates] = useState([]);
//     // const handleChooseAState = (event) => {
//     //     const value = event.target.value;
//     //     setStates(value);
//     // }
//     const [name, setName] = useState('');
//     const handleNameChange = (event) => {
//         const value = event.target.value;
//         setName(value);
//     }
//     const [roomCount, setRoomCount] = useState('');
//     const handleRoomCountChange = (event) => {
//         const value = event.target.value;
//         setRoomCount(value);
//     }
//     const [city, setCity] = useState('');
//     const handleCitychange = (event) => {
//         const value = event.target.value;
//         setCity(value);
//     }
//     const handleSubmit = async (event) => {
//         event.preventDefault();
//         const data = {};
//         data.room_count = roomCount;
//         data.name = name;
//         data.city = city;
//         data.states = states;
//         console.log(data);

//         const locationUrl = 'http://localhost:8000/api/locations/';
//         const fetchConfig = {
//             method: "post",
//             body: JSON.stringify(data),
//             headers: {
//                 'Content-Type': 'application/json',
//             }
//         };
//         const response = await fetch(locationUrl, fetchConfig);
//         if (response.ok) {
//             const newLocation = await response.json()
//             console.log(newLocation);

//             setName('');
//             setRoomCount('');
//             setCity('');
//             setState('');
//         }
//     }
//     const [state, setState] = useState('');
//     const handleChooseAState = (event) => {
//         const value = event.target.value;
//         setStates(value);
//     }
//     const fetchData = async () => {
//         const url = 'http://localhost:8000/api/states/';

//         const response = await fetch(url);

//         if (response.ok) {
//           const data = await response.json();
//           setStates(data.states);


//         }
//       }

//       useEffect(() => {
//         fetchData();
//       }, []);

//     return (
//         <div className="row">
//         <div className="offset-3 col-6">
//           <div className="shadow p-4 mt-4">
//             <h1>Create a new location</h1>
//             <form onSubmit={handleSubmit} id="create-location-form">
//               <div className="form-floating mb-3">
//                 <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
//                 <label htmlFor="name">Name</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input value={roomCount} onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control"/>
//                 <label htmlFor="room_count">Room count</label>
//               </div>
//               <div className="form-floating mb-3">
//                 <input value={city} onChange={handleCitychange} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
//                 <label htmlFor="city">City</label>
//               </div>
//               <div className="mb-3">
//                 <select value={state} onChange={handleChooseAState} required name="state" id="state" className="form-select">
//                   <option value="">Choose a state</option>
//                   {states.map(state => {
//                     return (
//                         <option key={state.abbreviation} value={state.abbreviation}>
//                             {state.name}
//                         </option>
//                     );
//                   })}
//                 </select>
//               </div>
//               <button className="btn btn-primary">Create</button>
//             </form>
//           </div>
//         </div>
//       </div>
//     );
// }

// export default LocationForm;
